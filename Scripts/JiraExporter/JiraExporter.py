from tkinter import *
from jira.client import JIRA
import xlsxwriter
from datetime import date


def MonthTextToNumber(Month):
    if Month == "Jan":
        return "01"
    elif Month == "Feb":
        return "02"
    elif Month == "Mar":
        return "03"
    elif Month == "Apr":
        return "04"
    elif Month == "May":
        return "05"
    elif Month == "Jun":
        return "06"
    elif Month == "Jul":
        return "07"
    elif Month == "Aug":
        return "08"
    elif Month == "Sep":
        return "09"
    elif Month == "Oct":
        return "10"
    elif Month == "Nov":
        return "11"
    elif Month == "Dec":
        return "12"
    else:
        return "Not a valid month: " + Month


# Button for password input
app = Tk()
identifier = StringVar(value='')
password = StringVar(value='')
sprintName = StringVar(value="R10 Sprint 19CW3")

Label(app, text="ID:").grid(row=0, column=0)
Label(app, text="Password:").grid(row=1, column=0)
Label(app, text="Week:").grid(row=3, column=0)

Entry(app, textvariable=identifier).grid(row=0, column=1)
Entry(app, textvariable=password, show='*').grid(row=1, column=1)
Entry(app, textvariable=sprintName).grid(row=3, column=1)

Button(app, text='Enter', command=app.destroy).grid(row=4, column=0, columnspan=2)
app.bind('<Return>', lambda e: app.destroy())
app.mainloop()

# Jira querry
url = 'https://trm.europe.prestagroup.com'
options = {'server': url, 'verify': False}
jql = 'assignee in (lorant.massar, milan2.szabo, andor.nemeth, tamas1.gulyas) AND sprint = \"' + sprintName.get() + "\""
jira = JIRA(options, basic_auth=(identifier.get(), password.get()))
issues = jira.search_issues(jql, maxResults=None)

# Find the current board and sprint
boards = jira.boards()
board = [b for b in boards if b.name == "R10 & R11 Board"][0]
sprints = jira.sprints(board.id)
sprint = [s for s in sprints if s.name == sprintName.get()][0]

# Get the start and end date of the selected sprint
sprintInfo = jira.sprint_info(board.id, sprint.id)

sprintStartDate = sprintInfo['startDate']
sprintStartDate = sprintStartDate[:sprintStartDate.index(" ")].split("/")
sprintEndDate = sprintInfo['endDate']
sprintEndDate = sprintEndDate[:sprintEndDate.index(" ")].split("/")

formattedIssueList = list()
formattedIssueList.append(["Component", "Task", "Assignee", "Status", "Estimated time", "D1",
                          "D2", "D3", "D4", "D5", "D6", "D7", "Summary", "Description", "Parent description", "Spent time"])
for index, currentIssue in enumerate(issues):
    timeLogsDays = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    assignee = currentIssue.fields.assignee.name
    timeEstimated = currentIssue.fields.aggregatetimeoriginalestimate/3600 if currentIssue.fields.aggregatetimeoriginalestimate is not None else 0
    subTask = currentIssue.fields.issuetype.name
    status = currentIssue.fields.status.name
    summary = currentIssue.fields.summary
    description = currentIssue.fields.description
    parentDescription = currentIssue.fields.customfield_11302

    # Get component name from the parent (Epic)
    parentIssue = ""
    component = ""
    if currentIssue.fields.customfield_10000 is not None:
        parentIssue = jira.issue(currentIssue.fields.customfield_10000)
        if parentIssue is not None:
            component = str(parentIssue.fields.components[0] if parentIssue.fields.components is not None else "")

    # Get work hours
    for worklog in jira.worklogs(currentIssue):
        timeSpent = int(worklog.timeSpentSeconds) / 3600
        dayOfLogging = worklog.started[:worklog.started.index("T")].split('-')  # first 10 characters are the year-month-day data

        # The last 2 digits of the year, the 2 digits of the month and two digits of the day is concatenated. For example: 2019 January 28 -> 190128
        concatenatedDayOfLogging    = int(dayOfLogging[0][2:]) * 100 * 100  + int(dayOfLogging[1]) * 100                        + int(dayOfLogging[2])
        concatenatedSprintStartDay  = int(sprintStartDate[2]) * 100 * 100   + int(MonthTextToNumber(sprintStartDate[1])) * 100  + int(sprintStartDate[0])
        concatenatedSprintEndDay    = int(sprintEndDate[2]) * 100 * 100     + int(MonthTextToNumber(sprintEndDate[1])) * 100    + int(sprintEndDate[0])

        # If the log date is in the sprint scope lets add the hours to the summarized times
        if (concatenatedSprintStartDay <= concatenatedDayOfLogging) and (concatenatedDayOfLogging <= concatenatedSprintEndDay):
            dayOfLoggingFormatted = date(int(dayOfLogging[0]), int(dayOfLogging[1]), int(dayOfLogging[2]))
            sprintStartDateFormatted = date(int('20' + sprintStartDate[2]), int(MonthTextToNumber(sprintStartDate[1])), int(sprintStartDate[0]))
            timeLogsDays[(dayOfLoggingFormatted-sprintStartDateFormatted).days] += timeSpent

    formattedIssueList.append([component, subTask, assignee, status, timeEstimated, timeLogsDays[0], timeLogsDays[1], timeLogsDays[2], timeLogsDays[3], timeLogsDays[4], timeLogsDays[5], timeLogsDays[6], summary, description, parentDescription, sum(timeLogsDays)])

# Create the excel report
workbook = xlsxwriter.Workbook(sprintName.get() + "_report.xlsx")
worksheet = workbook.add_worksheet("DefaultReport")

for row, line in enumerate(formattedIssueList):
    for column in range(len(formattedIssueList[row])):
        worksheet.write(row, column, formattedIssueList[row][column])

worksheet = workbook.add_worksheet("SprintPlannerReport")
workbook.close()
