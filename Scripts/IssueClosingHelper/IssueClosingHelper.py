from tkinter import *
import datetime
from jira.client import JIRA
import xlsxwriter

# Getting the current data data
currentYear, currentWeek, currentDay = datetime.date.today().isocalendar()

# GUI:
app = Tk()
identifier = StringVar(value='')
password = StringVar(value='')
issue = StringVar(value='123456')

Label(app, text="ID:").grid(row=0, column=0)
Label(app, text="Password:").grid(row=1, column=0)
Label(app, text="Issue:").grid(row=2, column=0)

Entry(app, textvariable=identifier).grid(row=0, column=1)
Entry(app, textvariable=password, show='*').grid(row=1, column=1)
Entry(app, textvariable=issue).grid(row=2, column=1)

Button(app, text='Enter', command=app.destroy).grid(row=4, column=0, columnspan=2)
app.bind('<Return>', lambda e: app.destroy())
app.mainloop()

# Jira querry
url = 'https://trm.europe.prestagroup.com'
options = {'server': url, 'verify': False}
jql = "\"IBM Change ID\" = " + issue.get()
jira = JIRA(options, basic_auth=(identifier.get(), password.get()))
issues = jira.search_issues(jql, maxResults=None)[0]

epics = []
for epic in issues.fields.issuelinks:
    # Only append the epics instead of everything
    if epic.type.name == "Component Developments":
        epics.append(epic)

sumTimes = []
svnLinks = []
doorsLinks = []
for epic in epics:
    sumTime = 0

    jql = "issueFunction in issuesInEpics(\"issue = " + epic.outwardIssue.key + "\")"
    subTasks = jira.search_issues(jql, maxResults=None)

    for task in subTasks:
        if task.fields.status.name not in ("Done", "Approved", "Canceled"):
            print("\n\nIssue: " + str(task.key) + " is not finished!")
            exit()

        if task.fields.aggregatetimespent is not None:
            sumTime += task.fields.aggregatetimespent/3600.0
        try:
            if task.fields.customfield_11300 is not None:
                svnLinks.append(task.fields.customfield_11300)
            if task.fields.customfield_11301 is not None:
                doorsLinks.append(task.fields.customfield_11301)
        except:
            pass    # There is no SvnLink and DoorsLink fields on this task. It is incorrectly created

    sumTimes.append(sumTime)

svnLinks = set(svnLinks).union()
doorsLinks = set(doorsLinks).union()

# Create the excel report
workbook = xlsxwriter.Workbook("CR" + issue.get() + '_report.xlsx')
worksheet = workbook.add_worksheet("DefaultReport")

worksheet.write(0, 0, "Time spent on this epic:")
worksheet.write(0, 1, "SVN links:")
worksheet.write(0, 2, "Doors links:")

for counter, time in enumerate(sumTimes):
    worksheet.write(1 + counter, 0, str(time))

for counter, currentLink in enumerate(svnLinks):
    worksheet.write(1 + counter, 1, str(currentLink))

for counter, currentLink in enumerate(doorsLinks):
    worksheet.write(1 + counter, 2, str(currentLink))

workbook.close()

print("\n\nFinished processing!\n")
input("Press Enter to exit...")
