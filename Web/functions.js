/* Filters the content of table based on the input of the search bar and the active filter button */
function inputFilter() 
{
	var input, filter, table, tr, td, i, rawScriptNameText, rawKeywordText, scriptNameText, keywordText;
	input = document.getElementById("myInput");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) 
	{
		rawScriptNameText = tr[i].getElementsByTagName("td")[0];
		rawKeywordText = tr[i].getElementsByTagName("td")[2];
		if (rawScriptNameText) 
		{
			scriptNameText = rawScriptNameText.textContent || rawScriptNameText.innerText;
			keywordText = rawKeywordText.textContent || rawKeywordText.innerText;
			if ((scriptNameText.toUpperCase().indexOf(filter) > -1) && (keywordText.toUpperCase().indexOf(keywordFilter.toUpperCase()) > -1))
			{
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}       
	}
}

/* Creates a list of the folders and files from the raw html data */
function parseDirectoryListing(text) 
{
	/* Finding all the possible directories and files in the folder */
	var regexpedArray = text.match(/a href="([^>]+)/g);
	
	/* Removing not necessary characters and the first and last elements fo the array because they are not real parts of the list */
	var formattedArray = regexpedArray.map((x) => x.replace('a href="', ''));
	formattedArray.splice(formattedArray.length - 1, 1);
	formattedArray.splice(0, 1);
	formattedArray = formattedArray.map((x) => x.replace('"', ''));
	formattedArray = formattedArray.map((x) => x.replace('/', ''));
	
	return String(formattedArray).split(",");
}   

/* Filters the table based on the curently active filter button */
function setKeywordFilter(button)
{
	document.getElementById("myInput").value = "";
	keywordFilter = button;
	filter = button.toUpperCase();
	table = document.getElementById("myTable");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) 
	{
		td = tr[i].getElementsByTagName("td")[2];
		if (td) 
		{
			txtValue = td.textContent || td.innerText;
			console.log("text: " + txtValue);
			console.log("filter: " + filter);
			if (txtValue.toUpperCase().indexOf(filter) > -1) 
			{
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}       
	}
	
	/* Getting the currently active button and replacing its class with "btn" */
	document.getElementsByClassName("active")[0].className = "btn";
	
	/* Activating the currently clicked button with adding the "active" class to it */
	this.className += " active";
}

/* Opens an alert message containing information about the usage of the tool and some contact data */
function helpButtonFunction()
{
	let alertMessage = "default";
	window.alert(alertMessage);
}